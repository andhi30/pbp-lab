# from django.http import response
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from lab_3.forms import FriendForm

# Create your views here.

@login_required(login_url="/admin/login/")
def index(request):
    response = {"friends": Friend.objects.all()}
    return render(request, "lab3_index.html", response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    response = {}
    form = FriendForm(request.POST or None)

    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect(reverse("index_lab3"))

    response = {"form": form}

    return render(request, "lab3_form.html", response)
