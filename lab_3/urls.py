from django.urls import path
from lab_3.views import add_friend, index

urlpatterns = [
    path('', index, name='index_lab3'),
    path('add', add_friend, name='add_friend'),
]
