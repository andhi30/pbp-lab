from datetime import date, datetime
from django.forms import *
from lab_1.models import Friend

class InputDate(DateInput):
    input_type = 'date'

# class InputNPM(NumberInput):
#     min

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'birth_date']

        error_messages = {
            'name': {
                'invalid': "Nama tidak valid", 
                'required': "Nama tidak boleh kosong"},
            'NPM' : {
                'invalid': "NPM tidak valid", 
                'required': "NPM tidak boleh kosong"},
            'birth_date' : {
                'invalid': "Tanggal lahir tidak valid", 
                'required': "Tanggal lahir tidak boleh kosong"},
        }

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Stiven Hocking'}),
            'npm': NumberInput(attrs={'placeholder': '2006293473'}),
            'birth_date': DateInput(attrs={'placeholder': date.today})
            }
