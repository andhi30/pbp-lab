from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=30)
    by = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    message = models.CharField(max_length=1000)
