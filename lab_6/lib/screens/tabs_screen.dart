// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';

import 'package:lab_6/models/forum.dart';
import 'package:lab_6/screens/forum_screen.dart';
import 'package:lab_6/screens/forum_detail_screen.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  int _selectedPageIndex = 0;
  static const List<Map<String, Object>> _pages = [
    {
      'page': ForumsScreen,
      'title': 'Events',
    },
    {
      'page': ForumsScreen,
      'title': 'Forum',
    },
    {
      'page': ForumsScreen,
      'title': 'Blog',
    },
    {
      'page': ForumsScreen,
      'title': 'Mini Survey',
    },
    {
      'page': ForumsScreen,
      'title': 'Account',
    },
  ];

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
      ),
      body: _pages[_selectedPageIndex]['pages'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        selectedItemColor: Theme.of(context).primaryColor,
        currentIndex: _selectedPageIndex,
        type: BottomNavigationBarType.shifting,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            label: 'Events',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.forum),
            label: 'Forum',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.group),
            label: 'Blog',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Mini Survey',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Account',
          ),
        ],
      ),
    );
  }
}
