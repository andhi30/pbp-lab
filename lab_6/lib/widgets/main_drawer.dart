import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key? key}) : super(key: key);

  Widget buildListTile(String title, Function tapHandler) {
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(
          fontFamily: 'RobotoCondesed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: const EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).primaryColor,
            child: Text(
              'Navigasi',
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 30,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          buildListTile('Home', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Events', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Forum', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Blog', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Mini Survey', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Login', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Sign Up', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
        ],
      ),
    );
  }
}
