import 'package:flutter/material.dart';

class Forum {
  final int id;
  final String author;
  String title;
  String body;
  final DateTime createdTime = DateTime.now();
  DateTime modifiedTime;

  Forum({
    required this.id,
    required this.author,
    required this.title,
    required this.body,
    required this.modifiedTime,
  });
}
