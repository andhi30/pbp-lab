import 'package:flutter/material.dart';
import 'package:lab_6/models/forum.dart';

// import './screens/tabs_screen.dart';
// import './screens/forum_screen.dart';
// import './screens/forum_detail_screen.dart';
// import './models/forum.dart';
import './dummy_data.dart';

void main() => runApp(const ForumApp());

class ForumApp extends StatefulWidget {
  const ForumApp({Key? key}) : super(key: key);

  @override
  _ForumAppState createState() => _ForumAppState();
}

class _ForumAppState extends State<ForumApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Forum',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
        ),
      ),
      home: _buildForum(),
    );
  }

  Widget _buildForum(forum) {
    return Card(
      margin: const EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Text(
            forum.title,
            style: const TextStyle(
              fontSize: 18,
            ),
          ),
          const SizedBox(height: 6),
          Text(
            forum.author,
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
          const SizedBox(height: 6),
          Text(
            forum.modifiedTime,
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
