from django.forms import *
from lab_2.models import Note

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        
        # to = CharField(
        #     label = 'To', 
        #     max_length = 5, 
        #     required = True, 
        #     widget = TextInput (
        #             attrs = {
        #                 'class':'form-control',
        #                 'placeholder':'Nama Lengkap',   
        #             }
        #         ),
        #     )
        
        # by = CharField(
        #     label = 'To', 
        #     max_length = 5, 
        #     required = True, 
        #     widget = TextInput (
        #             attrs = {
        #                 'class':'form-control',
        #                 'placeholder':'Nama Lengkap',   
        #             }
        #         ),
        #     )
        
        # title = CharField(
        #     label = 'Title', 
        #     max_length = 5, 
        #     required = True, 
        #     widget = TextInput (
        #             attrs = {
        #                 'class':'form-control',
        #                 'placeholder':'Judul',   
        #             }
        #         ),
        #     )

        # message = CharField(
        #     label = 'Message', 
        #     max_length = 1000, 
        #     required = True,
        #     widget = Textarea (
        #         attrs = {
        #             'class':'form-control',
        #             'placeholder':'Message',
        #             'oninvalid':'this.setCustomValidity("COOKKK")',
        #         }
        #     )
        # )

        
        # def __init__(self,*args, **kwargs):
        #     super(NoteForm, self).__init__(*args, **kwargs)

        #     for field in self.fields.values():
        #         field.error_messages = {
        #             'required':'Mohon isi {fieldname}'.format(fieldname = field.label)
        #             }       

        widgets = {
            'message': Textarea(),
        }

        oninvalids = {
            'to': 'setCustomValidity("COOOKKK")',
            'message': 'setCustomValidity("COOOKKK")',
        }