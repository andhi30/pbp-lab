from django.http import response
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls.base import reverse
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    context = {'notes': Note.objects.all()}
    return render(request, "lab4_index.html", context)
    
def note_list(request):
    context = {'notes': Note.objects.all()}
    return render(request, "lab4_note_list.html", context)

def add_note(request):
    form = NoteForm(request.POST or None)

    if (form.is_valid() and request.POST):
        form.save()
        print(form)
        return HttpResponseRedirect(reverse("index_lab4"))

    context = {"form": form}

    return render(request, "lab4_form.html", context)