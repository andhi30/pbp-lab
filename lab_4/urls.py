from django.urls import path
from lab_4.views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index_lab4'),
    path('add', add_note, name='add_note_lab4'),
    path('note-list', note_list, name='note_list_lab4'),
]
