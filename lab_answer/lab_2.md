#🔍 Jawaban Pertanyaan Singkat

## Q1: Apakah perbedaan antara JSON dan XML?

### JSON
- JavaScript Object Notation
- *Syntax* mirip dengan pemrograman Javascript
- Fokus utama menampilkan *objects*
- Tidak mendukung penggunaan [*namespace*](https://g.co/kgs/xRDV2X)
- Mendukung *array*
- Dokumen lebih sederhana sehingga mudah dibaca manusia
- Tidak menggunakan *tags*
- Lebih kurang aman
- Tidak mendukung *comments*
- Hanya mendukung *UTF-8 encoding*

### XML
- Extensible Markup Language
- Turunan dari [SGML](https://t.ly/fuT0T) (Standard Generalized Markup Language)
- Fokus utama *markup language* dan menggunakan struktur *tag* untuk menampikan data
- Mendukung *namespaces*
- Tidak mendukung *array*
- Dokumen lebih susah untuk dibaca
- Mempunyai *start* dan *end tags*
- Lebih aman
- Mendukung *comments*
- Mendukung berbagai macam *encoding*


## Q2: Apakah perbedaan antara HTML dan XML?

### HTML
- Hyper Text Markup Language
- *Static*
- *Markup language*
- Dapat mengabaikan *errors*
- Tidak *case sensitive*
- *Tags* merupakan *predefined*
- Jumlah *tags* terbatas
- *Tags* digunakan untuk menampilkan data
- Tidak wajib ada *Closing tags*
- Tidak *preserve white space*
- Digunakan untuk menampilkan data
- Tidak membawa data, hanya menampilkan saja

### XML
- eXtensible Markup Language
- *Dynamic*
- Menyediakan *framework* untuk mendefinisikan *markup languages*
- Tidak memperbolehkan *errors*
- *Case sensitive*
- *Tags* merupakan *user-defined*
- *Tags* dapat diperluas
- *Tags* digunakan untuk mendeskripsikan data, **bukan** untuk menampilkannya
- Wajib ada *Closing Tags*
- *Whitespace* dapat di-*preserved*
- Digunakan untuk menyimpan data
- Membawa data ke dan dari *database*


#### Referensi
1. https://www.geeksforgeeks.org/difference-between-json-and-xml/
2. https://www.geeksforgeeks.org/html-vs-xml/