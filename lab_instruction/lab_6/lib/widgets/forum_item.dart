// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';

import '../screens/forum_screen.dart';

class ForumItem extends StatelessWidget {
  String title;
  String author;
  DateTime modifiedTime;
  String body;

  ForumItem(this.title, this.author, this.modifiedTime, this.body);

  void selectCategory(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      ForumsScreen.routeName,
      arguments: {
        'id': id,
        'title': title,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.7),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
