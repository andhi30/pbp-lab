// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:lab_6/dummy_data.dart';

import '../models/forum.dart';

class ForumsScreen extends StatefulWidget {
  static const routeName = '/forums';
  @override
  _ForumsScreenState createState() => _ForumsScreenState();
}

class _ForumsScreenState extends State<ForumsScreen> {
  // String forumTitle;
  // List<Meal> displayedForums;
  // var _loadedInitData = false;

  // @override
  // void initState() {
  //   super.initState();
  // }

  // @override
  // void didChangeDependencies() {
  //   if (!_loadedInitData) {
  //     final routeArgs =
  //         ModalRoute.of(context).settings.arguments as Map<String, String>;
  //     forumTitle = routeArgs['title'];
  //     final categoryId = routeArgs['id'];
  //     displayedForums = widget.availableMeals.where((meal) {
  //       return meal.categories.contains(categoryId);
  //     }).toList();
  //     _loadedInitData = true;
  //   }
  //   super.didChangeDependencies();
  // }

  // void _removeMeal(String mealId) {
  //   setState(() {
  //     displayedForums.removeWhere((meal) => meal.id == mealId);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Column(
        children: DUMMY_FORUMS.map(
            (fData) => Forum(
                fData.id,
                fData.title
                fData.author,
                fData.modifiedTime,
                fData.body,
              ),
            ).toList(),
      ),
    );
}
